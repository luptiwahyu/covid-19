import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import sumBy from 'lodash/sumBy'
import uniqBy from 'lodash/uniqBy'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    summary: {
      loading: false,
      data: {}
    },
    global: {
      loading: false,
      data: []
    }
  },
  actions: {
    getGlobal ({ state, commit }) {
      state.global.loading = true
      axios
        .get('https://covid19.mathdro.id/api/confirmed')
        .then(result => {
          commit('setGlobal', result.data)
        })
    },
    getSummary ({ state, commit }) {
      state.summary.loading = true
      axios
        .get('https://covid19.mathdro.id/api')
        .then(result => {
          commit('setSummary', result.data)
        })
    }
  },
  mutations: {
    setGlobal (state, res) {
      let result = []

      res.forEach(item => {
        if (item.countryRegion === 'Korea, South') item.iso2 = 'KR'
        if (item.countryRegion === 'Cruise Ship') item.iso2 = '00'
        if (item.countryRegion === 'North Macedonia') item.iso2 = 'MK'
        if (item.countryRegion === 'Congo (Kinshasa)') item.iso2 = 'CD'
        if (item.countryRegion === 'Cote d\'Ivoire') item.iso2 = 'CI'
        if (item.countryRegion === 'Bahamas, The') item.iso2 = 'BS'
        if (item.countryRegion === 'Congo (Brazzaville)') item.iso2 = 'CD'
        if (item.countryRegion === 'Eswatini') item.iso2 = 'SZ'
        if (item.countryRegion === 'Gambia, The') item.iso2 = 'GM'
        if (item.countryRegion === 'Holy See') item.iso2 = 'VA'

        let filtered = res.filter(i => i.iso2 === item.iso2)
        let totalConfirmed = sumBy(filtered, 'confirmed')
        let totalRecovered = sumBy(filtered, 'recovered')
        let totalDeaths = sumBy(filtered, 'deaths')

        result.push({
          id: item.iso2,
          confirmed: totalConfirmed,
          recovered: totalRecovered,
          deaths: totalDeaths
        })
      })

      let countries = uniqBy(result, 'id')
      state.global.loading = false
      state.global.data = countries
    },
    setSummary (state, res) {
      state.summary.loading = false
      state.summary.data = res
    }
  }
})
